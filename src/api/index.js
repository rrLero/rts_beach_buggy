// @flow
import urlResolver from './urlResolver';
import {CONTENT_TYPE_JSON} from './const';
import 'isomorphic-fetch';

import type {ApiAction, Action, ApiDispatch, Dispatch, Store} from '../store/typedef';

const callApi = endpoint => endpoint()
    .then(response => {
        if (!response.ok) {
            return response.json().then(json => {
                return Promise.reject(json);
            });
        }
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.indexOf('application/json') !== -1) {
            return response.json().then(json => {
                return json;
            });
        } else {
            return response;
        }
    });

export default (store: Store) => (next: ApiDispatch | Dispatch) => (action: ApiAction | Action) => {
    const callAPI = action.CALL_API;
    if (typeof callAPI === 'undefined') {
        return next(action);
    }

    const {endpoint, types} = callAPI;

    const actionWith = data => {
        const finalAction = {...action, ...data};
        delete action.CALL_API;
        return finalAction;
    };

    const [requestType, successType, failureType] = types;
    next(actionWith({type: requestType}));

    return callApi(endpoint).then(
        response => next(actionWith({
            response,
            type: successType
        })),
        error => next(actionWith({
            type: failureType,
            error: error.message || error.errorMessage || 'Something bad happened'
        }))
    );
};

const headers = {'Content-Type': CONTENT_TYPE_JSON};


const getHeaders = () => {
    return headers;
};

const get = (url: string, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams), {headers: getHeaders()});

const getJWT = (url: string, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams));

const post = (url: string, body: {}, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams), {method: 'POST', headers: getHeaders(), body: JSON.stringify(body)});

const postExt = (url: string, body: {}, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams, false), {
        method: 'POST',
        body: JSON.stringify(body)
    });

const put = (url: string, body: {}, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams), {method: 'PUT', headers: getHeaders(), body: JSON.stringify(body)});

const patch = (url: string, body: {}, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams), {method: 'PATCH', headers: getHeaders(), body: JSON.stringify(body)});

const remove = (url: string, extraParams: {} = {}) =>
    fetch(urlResolver(url, extraParams), {method: 'DELETE', headers: getHeaders()});

export {get, getJWT, post, put, remove, patch, postExt};
