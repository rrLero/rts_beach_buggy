// @flow
const apiRoot = () => '/api/';

export default (url: string, extraParams: {}, isApi: boolean = true) => {
    const params = {...extraParams};
    return `${isApi ? apiRoot() : ''}${url}?${Object.entries(params)
        .map(([key, value]) => `${key}=${String(value)}`)
        .join('&')}`;
};

