// @flow

import {
    SET_ALERT
} from './constants';

import type {Dispatch} from 'src/store/typedef';
import type {AlertType} from './typedef';

export const setAlert = (alert: AlertType) => (dispatch: Dispatch) => {
    return dispatch({type: SET_ALERT, alert});
};
