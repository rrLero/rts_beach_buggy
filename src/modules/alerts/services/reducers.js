// @flow

import {
    SET_ALERT
} from './constants';

import type {AlertType} from './typedef';

const DEFAULT_STATE = {};

export type State = {
    alert?: AlertType
};

type Action =
    | { type: 'SET_ALERT', alert?: AlertType};

const alerts = (state: State = DEFAULT_STATE, action: Action): State => {

    if (action.type === SET_ALERT) {
        return {
            ...state,
            alert: action.alert
        };
    }

    return state;
};

export default alerts;
