// @flow
import React from 'react';
import {connect} from 'react-redux';

import AlertsView from './views';
import {setAlert} from './services/actions';

import type {AlertType} from './services/typedef';
import type {Dispatcher} from 'src/store/typedef';
import type {Classes} from './views/styles';

export type Props = {
    alert?: AlertType,
    setAlert: (error: AlertType) => Dispatcher,
    classes: Classes
};

const mapStateToProps = state => ({
    alert: state?.alerts?.alert
});

const mapDispatchToProps = {
    setAlert
};

const AlertsController = (props: Props) => {
    return (
        <AlertsView {...props}/>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(AlertsController);
