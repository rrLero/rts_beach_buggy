// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

import Snackbar from '@material-ui/core/Snackbar/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent/SnackbarContent';
import IconButton from '@material-ui/core/IconButton/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';

import type {Props} from '..';

const AlertsView = ({alert, classes, setAlert}: Props) => {

    return alert ? (
        <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center'
            }}
            open={!!alert}
            autoHideDuration={5000}
            onClose={() => setAlert('')}>
            <SnackbarContent
                className={classes.alerts}
                message={
                    <span className={classes.message}>
                        <ErrorIcon className={classes.icon} />
                        Alert: {alert}
                    </span>
                }
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={() => setAlert('')}>
                        <CloseIcon />
                    </IconButton>
                ]}
            />
        </Snackbar>
    ) : null;
};

export default withStyles(styles)(AlertsView);
