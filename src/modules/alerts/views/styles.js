// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    close: {
        padding: theme.spacing.unit / 2
    },
    alerts: {
        backgroundColor: theme.palette.primary.dark
    },
    icon: {
        fontSize: 20,
        marginRight: theme.spacing.unit
    },
    message: {
        display: 'flex',
        alignItems: 'center'
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
