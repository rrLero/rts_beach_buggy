// @flow
import React from 'react';
import {connect} from 'react-redux';

import ErrorsView from './views';
import {setError} from './services/actions';

import type {ErrorType} from './services/typedef';
import type {Dispatcher} from 'src/store/typedef';
import type {Classes} from './views/styles';

export type Props = {
    error?: ErrorType,
    setError: (error: ErrorType) => Dispatcher,
    classes: Classes
};

const mapStateToProps = state => ({
    error: state?.errors?.error
});

const mapDispatchToProps = {
    setError
};

const ErrorsController = (props: Props) => {
    return (
        <ErrorsView {...props}/>
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(ErrorsController);
