// @flow

import {
    SET_ERROR
} from './constants';

import type {ErrorType} from './typedef';

const DEFAULT_STATE = {};

export type State = {
    error?: ErrorType
};

type Action =
    | { type: 'SET_ERROR', error?: ErrorType};

const errors = (state: State = DEFAULT_STATE, action: Action): State => {

    if (action.type === SET_ERROR) {
        return {
            ...state,
            error: action.error
        };
    }

    return state;
};

export default errors;
