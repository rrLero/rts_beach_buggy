// @flow

import {
    SET_ERROR
} from './constants';

import type {Dispatcher} from 'src/store/typedef';
import type {ErrorType} from './typedef';

export const setError = (error: ErrorType): Dispatcher => dispatch => {
    return dispatch({type: SET_ERROR, error});
};
