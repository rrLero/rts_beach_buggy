// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

import Snackbar from '@material-ui/core/Snackbar/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent/SnackbarContent';
import IconButton from '@material-ui/core/IconButton/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';

import type {Props} from '..';

const ErrorsView = ({error, setError, classes}: Props) => {
    return error ? (
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'center'
            }}
            open={!!error}
            autoHideDuration={5000}
            onClose={() => setError('')}>
            <SnackbarContent
                className={classes.errors}
                message={
                    <span className={classes.message}>
                        <ErrorIcon className={classes.icon} />
                        ERROR: {error}
                    </span>
                }
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={() => setError('')}>
                        <CloseIcon />
                    </IconButton>
                ]}
            />
        </Snackbar>
    ) : null;
};

export default withStyles(styles)(ErrorsView);
