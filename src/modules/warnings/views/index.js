// @flow

import React from 'react';
import ConfirmWarning from './confirm-warning';
import Modal from '@material-ui/core/Modal/Modal';

import type {Props} from '..';

const WarningsView = ({warning}: Props) => {
    return (
        <Modal open={!!warning}>
            {warning && <ConfirmWarning warning={warning}/>}
        </Modal>
    );
};

export default WarningsView;
