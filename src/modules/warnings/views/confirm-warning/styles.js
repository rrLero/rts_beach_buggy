// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'absolute',
        width: theme.spacing.unit * 55,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 3,
        top: '40%',
        left: `calc(50% - ${theme.spacing.unit * 55 / 2}px)`,
        textAlign: 'center'
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
