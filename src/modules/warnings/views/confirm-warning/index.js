// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

import Typography from '@material-ui/core/Typography/Typography';
import Button from '@material-ui/core/Button/Button';

import type {WarningType} from 'src/modules/warnings/services/typedef';
import type {Classes} from './styles';

export type Props = {
    warning: WarningType,
    classes: Classes
};

const ConfirmWarningView = ({classes, warning}: Props) => {
    const {title, text, onApprove, onCancel} = warning;
    return (
        <div className={classes.paper}>
            <Typography variant="h6" id="modal-title" color="error" gutterBottom={true}>
                {title}
            </Typography>
            <Typography variant="subtitle1" id="simple-modal-description" gutterBottom={true}>
                {text}
            </Typography>
            <div>
                <Button onClick={onApprove} color="primary">Approve</Button>
                <Button onClick={onCancel} color="secondary">Cancel</Button>
            </div>
        </div>
    );
};

export default withStyles(styles)(ConfirmWarningView);
