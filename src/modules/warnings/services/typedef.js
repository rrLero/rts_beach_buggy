// @flow

export type WarningType = {
    title: string,
    text: string,
    onApprove: () => void,
    onCancel: () => void
};
