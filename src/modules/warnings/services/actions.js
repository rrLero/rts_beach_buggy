// @flow

import {
    SET_WARNING
} from './constants';

import type {Dispatcher} from 'src/store/typedef';
import type {WarningType} from './typedef';

export const setWarning = (warning: WarningType): Dispatcher => dispatch => {
    return dispatch({type: SET_WARNING, warning});
};
