// @flow

import {
    SET_WARNING
} from './constants';

import type {WarningType} from './typedef';

const DEFAULT_STATE = {};

export type State = {
    warning?: WarningType
};

type Action =
    | { type: 'SET_WARNING', warning?: WarningType};

const warnings = (state: State = DEFAULT_STATE, action: Action): State => {

    if (action.type === SET_WARNING) {
        return {
            ...state,
            warning: action.warning
        };
    }

    return state;
};

export default warnings;
