// @flow
import React from 'react';
import {connect} from 'react-redux';

import WarningsView from './views';

import type {WarningType} from 'modules/warnings/services/typedef';

export type Props = {
    warning?: WarningType
};

const mapStateToProps = state => ({
    warning: state?.warnings?.warning
});

const WarningController = (props: Props) => {
    return (
        <WarningsView {...props}/>
    );
};

export default connect(mapStateToProps, null)(WarningController);
