// @flow

import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';


const AllRoutes = () => {
    return (
        <Switch>
            <Route exact={true} path="/" component={() => <Redirect to="/devices"/>}/>
        </Switch>
    );
};

export default AllRoutes;

