// @flow

import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import green from '@material-ui/core/colors/green';
import blue from '@material-ui/core/colors/blue';
import red from '@material-ui/core/colors/red';
import yellow from '@material-ui/core/colors/yellow';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true
    },
    palette: {
        primary: {
            light: blue[700],
            main: blue[800],
            dark: blue[900]
        },
        secondary: {
            light: green[300],
            main: green[600],
            dark: green[700]
        },
        alert: {
            light: red[300],
            main: red[500],
            dark: red[700]
        },
        yellow: {
            light: yellow[300],
            main: yellow[500],
            dark: yellow[700]
        },
        mainBackground: 'rgb(238, 238, 238)'
    },
    myGlobals: {
        startColor: '#333333',
        startReversedColor: '#ffffff',
        mainFontSize: 16,
        drawerWidth: 200
    }
});

export default theme;

