// @flow

import React from 'react';
import Paper from '@material-ui/core/Paper/Paper';
import DataItem from './dataItem';

import type {Props} from '..';

const DataPopoverView = ({classes, data}: Props) => {
    return (
        <Paper className={classes.root} elevation={1}>
            <DataItem
                wifiName="Humidity, %"
                mac={`${data.humidity}`}
            />
            <DataItem
                wifiName="Temp, ℃"
                mac={`${data.temp}`}
            />
            <DataItem
                wifiName="Accelerometer"
                mac={`x: ${data.accelerometerX}, y: ${data.accelerometerY}, z: ${data.accelerometerZ}`}
            />
            <DataItem
                wifiName="Gyroscope"
                mac={`x: ${data.gyroscopeX}, y: ${data.gyroscopeY}, z: ${data.gyroscopeZ}`}
            />
            <DataItem
                wifiName="Magnetometer"
                mac={`x: ${data.magnetometerX}, y: ${data.magnetometerY}, z: ${data.magnetometerZ}`}
            />
            <DataItem
                wifiName="Compass"
                mac={`${data.compass}`}
            />
        </Paper>
    );
};

export default DataPopoverView;
