// @flow

import React from 'react';
import Typography from '@material-ui/core/Typography/Typography';

import type {Props} from '..';

const DataItemView = ({wifiName, mac, classes}: Props) => {
    return (
        <div className={classes.root}>
            <div className={classes.rootBody}>
                <Typography variant="body2" color="textSecondary">
                    {wifiName || 'NO DATA'}:
                </Typography>
                <div className={classes.divider}/>
                <Typography variant="body2" gutterBottom={true}>
                    {mac || 'NO DATA'}
                </Typography>
            </div>
        </div>
    );
};

export default DataItemView;
