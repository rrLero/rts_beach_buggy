// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from '../../../../../with-root/typedef';

const styles = (theme: Theme) => ({
    root: {
        '&:not(:first-child)::before': {
            content: '""',
            display: 'block',
            width: '100%',
            border: '1px solid lightgrey',
            marginBottom: 4
        }
    },
    rootBody: {
        display: 'flex'
    },
    divider: {
        marginRight: theme.spacing.unit
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
