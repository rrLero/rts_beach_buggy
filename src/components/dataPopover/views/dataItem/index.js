// @flow
import {compose} from 'recompose';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './views/styles';
import DataItemView from './views';

import type {HOC} from 'recompose';
import type {Classes} from './views/styles';

type OwnProps = {
    wifiName: string,
    mac: string
};
export type Props = {
    classes: Classes
} & OwnProps;

const hoc: HOC<Props, OwnProps> = compose(
    withStyles(styles)
);

export default hoc(DataItemView);

export {hoc};
