// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from '../../../with-root/typedef';

const styles = (theme: Theme) => ({
    root: {
        padding: theme.spacing.unit,
        paddingBottom: 0,
        width: '100%',
        border: '2px solid #00000059'
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
