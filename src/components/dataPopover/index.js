// @flow

import {compose} from 'recompose';
import withStyles from '@material-ui/core/styles/withStyles';

import styles from './views/styles';
import DataPopoverView from './views';

import type {HOC} from 'recompose';
import type {Classes} from './views/styles';
import type {EventdatasetDto} from '../../typedef/dto';

type OuterProps = {
    data: EventdatasetDto
};

type InnerProps = {
    classes: Classes
};

export type Props = InnerProps & OuterProps;

const hoc: HOC<Props, OuterProps> = compose(
    withStyles(styles)
);

export default hoc(DataPopoverView);
