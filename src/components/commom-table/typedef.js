// @flow

export type ActionType = {
    type: string,
    icon: string
};

export type DataTableFieldType = {
    [key: string]: {
        id?: number,
        data: string,
        link?: string,
        columnName?: string,
        popover?: string
    }
};

export type DataType = Array<DataTableFieldType>;

