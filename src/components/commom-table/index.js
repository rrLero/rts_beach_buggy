// @flow

import React from 'react';
import {NavLink, withRouter} from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import styles from './styles';
import Button from '@material-ui/core/Button/Button';
import Table from '@material-ui/core/Table/Table';
import TableBody from '@material-ui/core/TableBody/TableBody';
import TableFooter from '@material-ui/core/TableFooter/TableFooter';
import TablePagination from '@material-ui/core/TablePagination/TablePagination';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableRow from '@material-ui/core/TableRow/TableRow';
import Icon from '@material-ui/core/Icon/Icon';
import Paper from '@material-ui/core/Paper/Paper';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener/ClickAwayListener';

import EnhancedTableToolbar from './table-toolbar';
import EnhancedTableHead from './table-head';
import Pagination from './pagination';
import {SortOrder} from './constants';

import type {ActionType, DataTableFieldType} from './typedef';

type State = {
    order: $Values<typeof SortOrder>,
    orderBy: string,
    page: number,
    rowsPerPage: number,
    value: string,
    actionBy: { type: string, id: string | number | null },
    newItem: number,
    headsArray: Array<string>,
    openTooltip: {[key: number]: boolean}
};

type WithProps = {
    classes: $Call<typeof styles>
};

type OwnProps = {
    caption: string | React$Element<*>,
    data: Array<DataTableFieldType>,
    onAction: { type: string, id: string | number } => void,
    dataType: DataTableFieldType,
    actionsArray: Array<ActionType>,
    editable: boolean,
    addSelected: (Event, number) => void,
    selected: {[id: number]: boolean},
    noColumnFilter?: boolean,
    noFooter?: boolean
};

type Props = WithProps & OwnProps;

class EnhancedTable extends React.Component<Props, State> {

    constructor(props) {
        super(props);
        let data = {};
        const item = sessionStorage.getItem(props.actionsArray[0].type);
        if (item) {
            data = JSON.parse(item);
        }
        const {page, orderBy, order, rowsPerPage} = data;
        this.state = {
            order: order || SortOrder.DESC,
            orderBy: orderBy || Object.keys(this.props.dataType)[0],
            page: page | 0,
            rowsPerPage: rowsPerPage || 10,
            value: '',
            isModalOpened: false,
            actionBy: {id: null, type: ''},
            newItem: -2,
            headsArray: Object.keys(this.props.dataType),
            openTooltip: {}
        };
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        const order =
            (this.state.orderBy === orderBy && this.state.order === SortOrder.DESC)
                ? SortOrder.ASC
                : SortOrder.DESC;
        this.setState({order, orderBy});
        sessionStorage.setItem(
            `${this.props.actionsArray[0].type}`,
            JSON.stringify({order, orderBy, page: this.state.page, rowsPerPage: this.state.rowsPerPage})
        );
    };

    handleChangePage = (event, page) => {
        this.setState({page});
        const {order, orderBy, rowsPerPage} = this.state;
        sessionStorage.setItem(
            `${this.props.actionsArray[0].type}`,
            JSON.stringify({order, orderBy, page, rowsPerPage})
        );
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
        const {order, orderBy, page} = this.state;
        sessionStorage.setItem(
            `${this.props.actionsArray[0].type}`,
            JSON.stringify({order, orderBy, page, rowsPerPage: event.target.value})
        );
    };

    handleChangeSearchValue = value => {
        this.setState({value: value.toLowerCase()});
    };

    filterDataBySearchValue = data => {
        const {dataType} = this.props;
        const {orderBy, order, value} = this.state;
        const key = Object.keys(dataType)[0];
        const filterPredicate = a => ((a[key] && a[key].data) ? a[key].data.toLowerCase().includes(value) : a);
        const multiplier = order === SortOrder.DESC ? -1 : 1;

        const addOrder = data.map((el, i) => {
            return {i, data: el};
        });

        const sortAddOrder = addOrder.sort(((a, b) => {
            const bData = ((b.data[orderBy] && b.data[orderBy].data) || '');
            const aData = ((a.data[orderBy] && a.data[orderBy].data) || '');
            if (bData > aData) {
                return multiplier;
            }
            if (bData < aData) {
                return -multiplier;
            }
            return a.i - b.i;
        }));

        const preparedData = sortAddOrder.map(el => {
            return el.data;
        });

        return value ? preparedData.filter(filterPredicate) : preparedData;
    };

    getDateFromString = (date: string | number | null) => {
        if (!date) {
            return date;
        }
        const matchDate = date.toString().match(/^(\d+)-(\d+)-(\d+)/);
        if (!matchDate) {
            return date;
        }
        const dateNumber = Date.parse(matchDate[0]);
        const options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        return isNaN(dateNumber) ? date : new Date(dateNumber).toLocaleString('en-US', options);
    };

    getTableHead = () => {
        return this.state.headsArray.reduce((prev, curr) => {
            return {
                ...prev,
                [curr]: this.props.dataType[curr]
            };
        }, {});
    };

    handleTooltipClose = (i = 0) => () => {
        this.setState({openTooltip: {[i]: 'close'}});
    };

    handleTooltipOpen = (i = 0) => () => {
        this.setState({openTooltip: {[i]: 'open'}});
    };

    render() {
        const {
            classes, actionsArray, dataType, editable, data, selected, addSelected, noColumnFilter, caption
        } = this.props;
        const {order, orderBy, rowsPerPage, page, newItem} = this.state;
        const headsArray = this.state.headsArray;
        const preparedData = this.filterDataBySearchValue(data);
        const padding = 'none';
        return (
            <Paper className={classes.root}>
                {!noColumnFilter && (
                    <EnhancedTableToolbar
                        caption={caption}
                        handleChangeSearchValue={this.handleChangeSearchValue}
                    />
                )}
                <div className={classes.tableWrapper}>
                    <Table>
                        <EnhancedTableHead
                            order={(order === SortOrder.ASC || order === SortOrder.DESC) ? order : SortOrder.ASC}
                            orderBy={orderBy}
                            onRequestSort={this.handleRequestSort}
                            rowCount={data.length}
                            dataType={this.getTableHead()}
                            editable={editable}
                            selected={selected}
                            addSelected={addSelected}
                        />
                        <TableBody>
                            {preparedData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n, j) => {
                                const start = page * rowsPerPage + 1;
                                const idIndex = Object.keys(dataType)[0];
                                const tableCells = [
                                    <TableCell
                                        key={'number'}
                                        className={classes.tableCell}
                                        padding="dense">
                                        <div className={classes.checkCell}>
                                            {selected && addSelected && (
                                                <Checkbox
                                                    classes={{root: classes.checkbox}}
                                                    checked={!!selected[n[idIndex].id || -1]}
                                                    onChange={e => addSelected(e, n[idIndex].id || -1)}
                                                    color="primary"
                                                />
                                            )}
                                            {start + j}
                                        </div>
                                    </TableCell>,
                                    ...headsArray.map(columnName => {
                                        const stringFromIdField =
                                            this.getDateFromString((n[columnName] && n[columnName].id) || -1);
                                        const stringFromDataField
                                            = this.getDateFromString(n[columnName] && n[columnName].data);
                                        const link = dataType[columnName] && dataType[columnName].link;
                                        const popover = dataType[columnName] && dataType[columnName].popover;
                                        return (
                                            <TableCell
                                                padding={padding}
                                                key={`${stringFromDataField || ''} ${columnName}`}
                                                className={classes.tableCell}>
                                                {stringFromIdField && link && (
                                                    <NavLink
                                                        to={`/${link}/${stringFromIdField}`}>
                                                        {stringFromDataField}
                                                    </NavLink>
                                                )}
                                                {stringFromIdField && popover && (
                                                    <Tooltip
                                                        onClose={this.handleTooltipClose(n[columnName].id)}
                                                        open={this.state.openTooltip[+n[columnName].id] === 'open'}
                                                        disableFocusListener={true}
                                                        disableHoverListener={true}
                                                        disableTouchListener={true}
                                                        classes={{tooltip: classes.noMaxWidth, popper: classes.opacity}}
                                                        title={
                                                            <ClickAwayListener
                                                                onClickAway={this.handleTooltipClose(n[columnName].id)}>
                                                                {n[columnName] && n[columnName].popover}
                                                            </ClickAwayListener>
                                                        }>
                                                        <Button
                                                            color="primary"
                                                            variant="outlined"
                                                            onClick={this.handleTooltipOpen(n[columnName].id)}>
                                                            {n[columnName] && n[columnName].data}
                                                        </Button>
                                                    </Tooltip>
                                                )}
                                                {stringFromIdField && !link && !popover && stringFromDataField}
                                            </TableCell>
                                        );
                                    }),
                                    editable ?
                                        (
                                            <TableCell
                                                key={'action'}
                                                numeric={true}
                                                className={classes.tableCell}
                                                padding={padding}>
                                                {actionsArray.map(action => (
                                                    <Icon
                                                        key={action.icon}
                                                        className={classes.cursor}>
                                                        {action.icon}
                                                    </Icon>
                                                ))}
                                            </TableCell>
                                        ) : null
                                ];
                                return (
                                    <TableRow
                                        className={
                                            classNames(
                                                classes.tableRow,
                                                +newItem === +n[headsArray[0]].id ? classes.newItem : ''
                                            )}
                                        hover={true}
                                        role="checkbox"
                                        tabIndex={-1}
                                        key={(n[headsArray[0]] && n[headsArray[0]].id) || j}>
                                        {tableCells}
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                        {!this.props.noFooter && (
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        colSpan={6}
                                        count={preparedData.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        backIconButtonProps={{
                                            'aria-label': 'Previous Page'
                                        }}
                                        nextIconButtonProps={{
                                            'aria-label': 'Next Page'
                                        }}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        ActionsComponent={Pagination}
                                    />
                                </TableRow>
                            </TableFooter>
                        )}
                    </Table>
                </div>
            </Paper>
        );
    }
}

const withStyleConnector = withStyles(styles);

export default withRouter(withStyleConnector(EnhancedTable));
