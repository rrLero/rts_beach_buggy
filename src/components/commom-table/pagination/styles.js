// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from '../../../with-root/typedef';

const styles = (theme: Theme) => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5
    }
});

export default styles;
