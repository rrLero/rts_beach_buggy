// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import Typography from '@material-ui/core/Typography/Typography';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';

import TableSearch from '../../search';

import styles from './styles';

type WithProps = {
    classes: $Call<typeof styles>
};

type OwnProps = {
    caption: string | React$Element<*>,
    handleChangeSearchValue: string => void
};

type Props = WithProps & OwnProps;

const EnhancedTableToolbar = ({classes, caption, handleChangeSearchValue}: Props) => {

    return (
        <Toolbar
            className={classes.root}>
            <div className={classes.title}>
                {typeof caption === 'string' ? (
                    <Typography variant="h6">{caption}</Typography>
                ) : caption}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                <Tooltip title="Filter list">
                    <TableSearch handleChangeSearchValue={handleChangeSearchValue}/>
                </Tooltip>
            </div>
        </Toolbar>
    );
};

const withStyleConnector = withStyles(styles);

export default withStyleConnector(EnhancedTableToolbar);
