// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from '../../../with-root/typedef';
import {lighten} from '@material-ui/core/styles/colorManipulator';

const styles = (theme: Theme) => ({
    root: {
        paddingRight: theme.spacing.unit
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85)
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark
            },
    spacer: {
        flex: '1 1 100%'
    },
    actions: {
        color: theme.palette.text.secondary
    },
    title: {
        flex: '0 0 auto',
        textTransform: 'uppercase',
        marginTop: theme.spacing.unit * 3
    }
});

export default styles;
