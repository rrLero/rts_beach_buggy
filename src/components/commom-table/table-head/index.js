// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableHead from '@material-ui/core/TableHead/TableHead';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import type {DataTableFieldType} from '../typedef';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';

type WithProps = {
    classes: $Call<typeof styles>
};

type OwnProps = {
    onRequestSort: (Event, string) => void,
    order: string,
    orderBy: string,
    rowCount: number,
    dataType: DataTableFieldType,
    editable: boolean,
    addSelected?: (Event, number | 'all') => void,
    selected?: {[id: number | 'all']: boolean}
};

type Props = WithProps & OwnProps;

const EnhTableHead = ({
    onRequestSort, editable, order, orderBy, rowCount, dataType, sortHandler, classes, selected, addSelected
}: Props) => {
    const createObj = el => ({id: el, numeric: false, disablePadding: true, label: dataType[el].columnName});
    const columnData = Object.keys(dataType).map(createObj);
    const rows = columnData.length || 1;
    return (
        <TableHead>
            <TableRow>
                <TableCell style={{width: '5%'}}>
                    <div className={classes.checkCell}>
                        {selected && addSelected && (
                            <Checkbox
                                classes={{root: classes.checkbox}}
                                checked={!!selected.all}
                                onChange={e => addSelected(e, 'all')}
                                color="primary"
                            />
                        )}
                        #
                    </div>
                </TableCell>
                {columnData.map(column => {
                    return (
                        <TableCell
                            style={{width: `${90 / rows}%`}}
                            key={column.id}
                            padding={column.disablePadding ? 'none' : 'default'}
                            sortDirection={orderBy === column.id ? order : false}>
                            <Tooltip
                                title="Sort"
                                placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                enterDelay={300}>
                                <TableSortLabel
                                    active={orderBy === column.id}
                                    direction={order}
                                    onClick={event => onRequestSort(event, column.id)}
                                    className={classes.uppercase}>
                                    {column.label}
                                </TableSortLabel>
                            </Tooltip>
                        </TableCell>
                    );
                })}
            </TableRow>
        </TableHead>
    );
};

const withStyleConnector = withStyles(styles);

export default withStyleConnector(EnhTableHead);
