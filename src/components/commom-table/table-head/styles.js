// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from '../../../with-root/typedef';

const styles = (theme: Theme) => ({
    uppercase: {},
    textRight: {
        textAlign: 'right'
    },
    checkCell: {
        display: 'flex',
        alignItems: 'center'
    },
    checkbox: {
        padding: '0 12px'
    }
});

export default styles;
