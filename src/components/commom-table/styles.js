// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from '../../with-root/typedef';

const styles = (theme: Theme) => ({
    root: {
        width: '100%'
    },
    tableCell: {
        textTransform: 'capitalize',
        whiteSpace: 'nowrap',
        paddingRight: theme.spacing.unit * 3
    },
    tableWrapper: {
        overflowX: 'auto'
    },
    cursor: {
        cursor: 'pointer'
    },
    tableRow: {
        transition: 'background-color 0.5s',
        '&:nth-child(2n)': {
            backgroundColor: 'rgba(0, 0, 0, 0.07)'
        },
        '&:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.15)!important'
        }
    },
    newItem: {
        backgroundColor: 'rgba(0,0,0,0.3)'
    },
    checkCell: {
        display: 'flex',
        alignItems: 'center'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300
    },
    noMaxWidth: {
        maxWidth: 450,
        background: 'none',
        boxShadow: theme.shadows[23],
        padding: 0,
        border: '1px solid gray'
    },
    opacity: {
        opacity: 1
    },
    checkbox: {
        padding: '0 12px'
    }
});

export default styles;
