// @flow

export const SortOrder = {
    ASC: 'asc',
    DESC: 'desc'
};
