// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import styles from './styles';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import TextField from '@material-ui/core/TextField/TextField';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment/InputAdornment';

type WithProps = {
    classes: $Call<typeof styles>
};

type OwnProps = {
    handleChangeSearchValue: string => void
};

type Props = WithProps & OwnProps;

const EnhTableHead = ({classes, handleChangeSearchValue}: Props) => {
    return (
        <Tooltip
            title="Search"
            placement={'bottom-end'}
            enterDelay={300}>
            <TextField
                id="search"
                label="Search"
                type="search"
                InputProps={{
                    endAdornment: <InputAdornment position="end"><SearchIcon/></InputAdornment>
                }}
                margin="normal"
                onChange={event => handleChangeSearchValue(event.target.value)}
            />
        </Tooltip>
    );
};

const withStyleConnector = withStyles(styles);

export default withStyleConnector(EnhTableHead);
