// @flow

import React from 'react';
import CommonView from '../common-view';
import AllRoutes from '../../routes';

const PrimaryLayout = () => {
    return (
        <CommonView>
            <AllRoutes/>
        </CommonView>
    );
};

export default PrimaryLayout;
