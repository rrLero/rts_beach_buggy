// @flow

import React from 'react';
import {BrowserRouter, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import withRoot from '../with-root';
import store from '../store';

import PrimaryLayout from './primary-layout';

const App = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <PrimaryLayout/>
                </Switch>
            </BrowserRouter>
        </Provider>
    );
};

export default withRoot(App);

