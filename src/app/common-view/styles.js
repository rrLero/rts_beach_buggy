// @flow
/* eslint quote-props: ["error", "as-needed"] */
import background from './images/background.webp';
import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    root: {
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        background: `url(${background})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
    },
    header: {

    },
    main: {
        flexGrow: 1,
        display: 'flex',
        overflow: 'hidden'
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar
    },
    children: {
        flexGrow: 1,
        marginTop: theme.spacing.unit * 12,
        display: 'flex',
        flexDirection: 'column',
        maxWidth: `calc(100% - ${theme.spacing.unit * 9}px)`,
        paddingBottom: theme.spacing.unit * 2
    },
    children__open: {
        maxWidth: `calc(100% - ${theme.myGlobals.drawerWidth}px)`
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
