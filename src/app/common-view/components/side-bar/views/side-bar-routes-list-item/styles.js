// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    active: {
        backgroundColor: theme.palette.action.selected
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
