// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import {NavLink} from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import Avatar from '@material-ui/core/Avatar/Avatar';

import type {Node} from 'react';
import type {Classes} from './styles';

type OwnProps = {
    title: string,
    path: string,
    icon: Node
};
type RestProps = {
    classes: Classes
};
export type Props = OwnProps & RestProps;

const SideBarRoutesListItemView = ({classes, title, path, icon}: Props) => {
    return (
        <ListItem button={true} component={props => (
            <NavLink to={path} activeClassName={classes.active} {...props}/>
        )}>
            <ListItemAvatar>
                <Avatar
                    alt="Icon"
                    src={icon}
                />
            </ListItemAvatar>
            <ListItemText primary={title}/>
        </ListItem>
    );
};

export default withStyles(styles)(SideBarRoutesListItemView);
