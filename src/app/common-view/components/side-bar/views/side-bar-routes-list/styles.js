// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    root: {
        width: '100%',
        maxWidth: 360
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
