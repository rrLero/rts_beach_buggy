// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

import List from '@material-ui/core/List/List';

import type {HOC} from 'recompose';
import type {Node} from 'react';
import type {Classes} from './styles';

type OwnProps = {
    children: Node
};
type RestProps = {
    classes: Classes
};

type Props = OwnProps & RestProps;

const SideBarRoutesListView = ({classes, children}: Props) => {
    return (
        <div className={classes.root}>
            <List component="nav">
                {children}
            </List>
        </div>
    );
};

export default withStyles(styles)(SideBarRoutesListView);
