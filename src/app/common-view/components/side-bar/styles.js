// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    drawerPaper: {
        position: 'relative',
        width: theme.myGlobals.drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        }),
        background: theme.palette.yellow.main
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9
        }
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
