// @flow

import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

import tournamentsImage from './images/tournaments.webp';
import ratingImage from './images/rating.webp';

import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Drawer from '@material-ui/core/Drawer/Drawer';
import Divider from '@material-ui/core/Divider/Divider';

import SideBarRoutesList from './views/side-bar-routes-list';
import SideBarRoutesListItem from './views/side-bar-routes-list-item';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import type {Classes} from './styles';
import type {Theme} from 'src/with-root/typedef';

type OwnProps = {
    onClick: () => void,
    open: boolean,
    toolbarClass: {}
};
type RestProps = {
    classes: Classes,
    theme: Theme
};

type Props = OwnProps & RestProps;

const View = ({classes, onClick, open, theme, toolbarClass}: Props) => {
    return (
        <Drawer
            variant="permanent"
            classes={{
                paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose)
            }}
            open={open}>
            <div className={toolbarClass}>
                <IconButton onClick={onClick}>
                    {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                </IconButton>
            </div>
            <Divider />
            <SideBarRoutesList>
                <SideBarRoutesListItem title="Tournaments" path="/tournaments" icon={tournamentsImage}/>
                <SideBarRoutesListItem title="Rating" path="/rating" icon={ratingImage}/>
            </SideBarRoutesList>
        </Drawer>
    );
};

export default withStyles(styles, {withTheme: true})(View);
