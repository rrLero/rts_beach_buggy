// @flow

import React from 'react';
import {NavLink} from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';
import classNames from 'classnames';

import AppBar from '@material-ui/core/AppBar/AppBar';
import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Typography from '@material-ui/core/Typography/Typography';

import type {Classes} from './styles';

type Props = {
    onClick: () => void,
    open: boolean,
    classes: Classes
};

const HeaderView = ({classes, onClick, open}: Props) => {
    return (
        <AppBar
            color="primary"
            position="absolute"
            className={classNames(classes.appBar, open && classes.appBarShift)}>
            <Toolbar disableGutters={!open} className={classes.toolbar}>
                <IconButton
                    onClick={onClick}
                    color="inherit"
                    className={classNames(classes.menuButton, open && classes.hide)}>
                    <MenuIcon />
                </IconButton>
                <div className={classes.flex}>
                    <NavLink to="/">
                        <Typography
                            color="inherit"
                            variant="h6"
                            noWrap={true}>
                            Beach - Buggy
                        </Typography>
                    </NavLink>
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default withStyles(styles)(HeaderView);
