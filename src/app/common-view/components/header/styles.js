// @flow
/* eslint quote-props: ["error", "as-needed"] */

import {type Theme} from 'src/with-root/typedef';

const styles = (theme: Theme) => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        marginLeft: theme.myGlobals.drawerWidth,
        width: `calc(100% - ${theme.myGlobals.drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    menuButton: {
        marginLeft: 13,
        marginRight: 36
    },
    hide: {
        display: 'none'
    },
    flex: {
        flex: 1,
        display: 'flex',
        '& a': {
            display: 'flex',
            flexWrap: 'nowrap',
            alignItems: 'center',
            textDecoration: 'none',
            color: '#FFFFFF',
            '&:hover': {
                opacity: 0.8,
                color: '#FFFFFF'
            },
            transition: 'opacity 0.3s'
        }
    },
    toolbar: {
        paddingRight: theme.spacing.unit * 3
    },
    image: {
        maxHeight: theme.spacing.unit * 4
    }
});

export default styles;

export type Classes = $Call<typeof styles>;
