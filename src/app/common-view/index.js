// @flow

import React, {useState} from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

import classNames from 'classnames';
import Header from './components/header';
import SideBar from './components/side-bar';
import Warnings from 'modules/warnings';
import Errors from 'modules/errors';
import Alerts from 'src/modules/alerts';

import type {Node} from 'react';
import type {Classes} from './styles';

type Props = {
    open: boolean,
    handleOpen: () => void,
    classes: Classes,
    children: Node
};

const CommonViewView = ({classes, children}: Props) => {

    const [open, handleOpen] = useState(false);

    return (
        <div className={classes.root}>
            <header className={classes.header}>
                <Header
                    open={open}
                    onClick={() => handleOpen(!open)}
                />
            </header>
            <main className={classes.main}>
                <SideBar
                    open={open}
                    onClick={() => handleOpen(!open)}
                    toolbarClass={classes.toolbar}
                />
                <div className={classNames(classes.children, open && classes.children__open)}>
                    {children}
                </div>
            </main>
            <Warnings/>
            <Errors/>
            <Alerts/>
        </div>
    );
};

export default withStyles(styles)(CommonViewView);
