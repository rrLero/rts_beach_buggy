// @flow

export type UserDto = {
    id: number,
    password: string,
    login: string,
    isAdmin: boolean,
    createdAt?: string,
    updatedAt?: string
};

export type DeviceDto = {
    id: number,
    deviceId: string,
    name: string,
    userId: number,
    createdAt: string,
    updatedAt: string,
    Setting: SettingsDto
};

export type SettingsDto = {
    id: number,
    deviceId: number,
    transmissionBattery: number,
    transmissionMain: number,
    gpsBattery: number,
    gpsMain: number,
    checkSensorsBattery: number,
    checkSensorsMain: number,
    attemptsGPSBattery: number,
    attemptsGPSMain: number,
    attemptsSatelliteBattery: number,
    attemptsSatelliteMain: number,
    createdAt: string,
    updatedAt: string
};

export type EventDto = {
    id: number,
    packetId: number,
    charge: number,
    date: string,
    packageType: string,
    signal: number,
    cordsType: boolean,
    latitude: number,
    longitude: number,
    accuracy: number,
    createdAt: string,
    updatedAt: string,
    deviceId: number,
    Eventdataset: EventdatasetDto,
    Device?: DeviceDto
};

export type EventdatasetDto = {
    id: number,
    accelerometerX: number,
    accelerometerY: number,
    accelerometerZ: number,
    gyroscopeX: number,
    gyroscopeY: number,
    gyroscopeZ: number,
    magnetometerX: number,
    magnetometerY: number,
    magnetometerZ: number,
    temp: number,
    humidity: number,
    compass: string,
    eventId: number
};
