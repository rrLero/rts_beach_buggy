// @flow

import {combineReducers} from 'redux';
import warnings from 'modules/warnings/services/reducers';
import errors from 'modules/errors/services/reducers';
import alerts from 'modules/alerts/services/reducers';

import type {State as WarningsState} from 'modules/warnings/services/reducers';
import type {State as ErrorsState} from 'modules/errors/services/reducers';
import type {State as AlertsState} from 'modules/alerts/services/reducers';

export type State = {|
    warnings: WarningsState,
    errors: ErrorsState,
    alerts: AlertsState
|};

export default combineReducers<Object, *>({ // eslint-disable-line
    warnings,
    errors,
    alerts
});
