const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.config');

module.exports = webpackMerge(commonConfig, {
    mode: 'production',
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    }
});
