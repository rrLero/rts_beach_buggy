const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    cache: true,
    entry: {
        app: './src/index.js',
        polyfills: 'babel-polyfill'
    },
    output: {
        filename: '[name].bundle.[hash].js',
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/'
    },
    resolve: {
        extensions: [
            '.js'
        ],
        alias: {
            modules: path.resolve('src/modules/'),
            components: path.resolve('src/components/'),
            src: path.resolve('src/')
        }
    },
    devServer: {
        port: 8080,
        open: true,
        hot: false,
        inline: false,
        proxy: {
            '/api': 'http://localhost:3000'
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,
            template: './public/index.html',
            favicon: './public/favicon.ico'
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                include: path.resolve()
            },
            {
                test: /\.(png|jpg|gif|svg|webp)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: './images/[name].[ext]'
                    }
                }
            }
        ]
    }
};
