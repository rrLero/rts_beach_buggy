import express from 'express';
import path from 'path';

const app = express();

app.use('/', express.static(path.join(__dirname, 'dist')));

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`App available on ${port}`); // eslint-disable-line no-console
});

module.exports = app;
